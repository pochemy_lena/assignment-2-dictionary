%include "lib.inc"
%include "dict.inc"
%include "words.inc"

section .rodata
%define size_buff 256
%define qword_size 8

global _start

section .data
input_error: db "Invalid input", 0
not_found: db "Key not found", 0

section .bss
buff: resb size_buff	;выделяем буфер

section .text

_start:		
	mov rdi, buff 		;адрес начала буфера
	mov rsi, size_buff		;размер буфера
	call read_line 		; При успехе возвращает адрес буфера в rax, длину слова в rdx. При неудаче возвращает 0 в rax

	test rax, rax 
	jz .input_error 	;если 0, то не получилось прочитать

	mov rdi, buff 		;указатель на строку
	mov rsi, next 		;указатель на словарь
	call find_word		;адрес начала вхождения в словарь -> rax

	test rax, rax
	jz .not_found 		;если вернулся 0, то слово не найдено в словаре

	mov rdi, rax       ;адрес начала вхождения в словарь
	add rdi, qword_size     ;переходим к метке
	push rdi
	call string_length ;считаем длину строки
	pop rdi
	add rdi, rax       ;конец строки
	inc rdi            ;пропускаем нуль-терминант

	.end_ok:
		call print_string       ;выводим значение или сообщение об ошибке
        call print_newline      ;выводим переход на новую строку для красоты      
		xor rdi, rdi
		call exit

	.input_error:
		mov rdi, input_error      ;сообщение об ошибке ввода
		jmp .end_error

	.not_found:
		mov rdi, not_found        ;сообщение об ошибке - ключ не найден
	
	.end_error:
		call print_error       ;выводим значение или сообщение об ошибке
        call print_newline      ;выводим переход на новую строку для красоты      
		mov rdi, 1
		call exit
