%include "lib.inc"

;макросы, которые кладут на стек и снимают с него любое количество регистров:

%macro push_regs 1-* 
  %rep %0
    push %1
    %rotate 1
  %endrep
%endmacro

%macro pop_regs 1-*
  %rep %0
    pop %1
    %rotate 1
  %endrep
%endmacro

global find_word

section .rodata
%define qword_size 8

section .text
;find_word пройдёт по всему словарю в поисках подходящего ключа. Если подходящее вхождение найдено, вернёт адрес начала вхождения в   словарь (не значения), иначе вернёт 0.
;rdi - указатель на строку, rsi - указатель на начало словаря

find_word: 

    push_regs r12, r13
    mov r13, rdi
    .loop:
        test rsi, rsi       ;проверка на конец словаря
        jz .not_found       
        mov r12, rsi
        
        add rsi, qword_size         ;переходим к метке
        mov rdi, r13
                            ;rsi - слово в словаре, rdi - данное слово
        call string_equals  ;сравниваем строки, возвращает 1 если они равны, 0 иначе

        test rax, rax       
        jnz .found          ;если не 0, то строки равны, то есть слово есть в словаре

        mov rsi, [r12]      ;к след слову в словаре
        jmp .loop

    .found:
        mov rax, r12        ;возвращаем адрес начала вхождения слова в словарь
        jmp .ret

    .not_found:
        xor rax, rax        ;возвращаем 0

    .ret:
        pop_regs r13, r12
        ret
