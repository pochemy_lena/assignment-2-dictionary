#!/bin/bash

keys=([1]="key" [2]="third word" [3]="lalala" [4]="qwertyuiuytrfedswadfghjkjhgfdcxzxcvbnmkiuytredcfvgbhnjuqwertyuiasdfghjklzxcvbnmq1wertyuiuyhgtfrdesasdfghjnbvcxqwertyuioasdfghjklzxcvbnmqwertyuioasdfghjkzxcvbnmqwertyuikolasdfghjklxcvbnmqwertyuikoasdfgbhnjmkxcvbnmqwertyuiopasdfghjklzxcvbnmwertyhujkljhgfdsxcvbn")

expected=([1]="key2" [2]="third word explanation" [3]="Key not found" [4]="Invalid input")
passed_test=0

for i in ${!keys[*]}
do
	printf "Тест %d:\n" $i
	res=$(echo ${keys[$i]} | ./main 2>&1)
	exp=${expected[$i]}
	echo "Дано:"
	echo ${keys[$i]}
	echo "Ожидаемый результат:"
	echo $exp
	echo "Результат:"
	echo $res
	if [ "$res" == "$exp" ] 
	then 
		printf "Тест %d пройден\n" $i
		passed_test=$(($passed_test +1))
	else
		printf "Тест %d не пройден\n" $i
	fi
	echo "------------------------------------"
done

printf "Пройдено %d из 4 тестов " $passed_test
