ASM = nasm
ASMFLAGS = -felf64


%.o: %.asm
	$(ASM) $(ASMFLAGS) -o $@ $<

dict.o: dict.asm lib.inc

main.o: main.asm lib.inc dict.inc words.inc

main: main.o lib.o dict.o
	ld -o main $^

.PHONY: go test

go: main
	./main
	
test: test2.sh
	./test2.sh
